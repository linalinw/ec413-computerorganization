############################################################################
#                       lab 3
#                       EC413
#
#           Assembly Language Lab -- Programming with Loops.
#
############################################################################
#  DATA
############################################################################
        .data           # Data segment
Hello:  .asciiz " \n Hello World! \n "  # declare a zero terminated string
Hello_len: .word 16
AnInt:  .word   12      # a word initialized to 12
space:  .asciiz " " # declare a zero terminate string
WordAvg:   .word 0      #use this variable for part 4
ValidInt:  .word 0      # 
ValidInt2: .word 0      # 
lf:     .byte   10, 0   # string with carriage return and line feed
InLenW: .word   4       # initialize to number of words in input1 and input2
InLenB: .word   16      # initialize to number of bytes in input1 and input2
        .align  4 
Input1: .word   0x01020304,0x05060708
        .word   0x090A0B0C,0x0D0E0F10
        .align  4 
Input2: .word   0x01221117,0x090b1d1f   # input
        .word   0x0e1c2a08,0x06040210
        .align  4 
Copy:   .space  0x80    # space to copy input word by word
        .align  4
 
Enter: .asciiz "\n"
Comma: .asciiz "," 
############################################################################
#  CODE
############################################################################
        .text                   # code segment
#
# print out greeting
#
main:
        la  $a0,Hello   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
 
 
#Code for Item 2
#Count number of occurences of letter "l" in Hello string
#
li $t0, 0 #counter
li $s0, 0 #number of l in the sentence
lw $t1, Hello_len #stores the length of Hello
la $t2, Hello #stores the address of the first byte of Hello
li $t4, 108 #ascii number of 'l'

loopone:
    beq $t1,$t0,endone #check if end of the Hello is read
    add $t0,$t0,1 #counter
    lb  $t3,($t2) #loads the byte of at current address which corresponds to ascii number
    add $t2,$t2,1 #move the pointer up Hello
    beq $t3,$t4,IFONE #checks if each character is 'l'
    j loopone
IFONE:
    add,$s0,$s0,1 #increment counter if 'l' character is counter
    j loopone
endone:
    sw $s0,WordAvg
 
################################################################################
        lw  $a0,WordAvg
        li  $v0,1       # system call code for print_str
        syscall                 # print the string
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
################################################################################

#
# Code for Item 3 -- 
# Print the integer value of numbers from 0 and less than AnInt
#
lw $t0, AnInt
li $t1, 0
sw $t1, ValidInt
looptwo:
    beq $t0,$t1,endtwo #check that current number is less than AnInt
    lw  $a0,ValidInt
    li  $v0,1
    syscall
    la  $a0,space #print space
    li  $v0,4
    syscall #print the numbers
    add $t1,$t1,1
    sw  $t1,ValidInt
    j   looptwo
endtwo:

###################################################################################
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
###################################################################################
#
# Code for Item 4 -- 
# Print the integer values of each byte in the array Input1 (with spaces)
#
la  $t0, Input1 #store the address of the first byte in the array Input1
lw  $t1, InLenB #store the length in byte of array Input1
li  $t2, 0 #counter
loopthree:
    beq $t1,$t2,endthree
    lb  $s0, ($t0) #this is not working
    sw  $s0, ValidInt2
    lw  $a0, ValidInt2
    li  $v0,1
    syscall
    la  $a0,space
    li  $v0,4
    syscall
    add $t2,$t2,1
    add $t0,$t0,1
    j   loopthree
endthree:

#################################################################################
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
###################################################################################
#
###################################################################################
#
# Code for Item 5 -- 
# Write code to copy the contents of Input2 to Copy
#
la  $s0,Input2
la  $s1,Copy
lw  $t2,InLenW
li  $t1,0
loopfour:
    beq $t1,$t2,endfour
    lw  $t0,($s0)
    sw  $t0,0($s1)
    add $s0,$s0,4
    add $s1,$s1,4
    add $t1,$t1,1
    j   loopfour
endfour:
#################################################################################
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
###################################################################################
#
# Code for Item 6 -- 
# Print the integer average of the contents of array Input2
#
MIN: .word  0
MAX: .word  0
lw  $t3,MIN
lw  $t4,MAX
la  $s0,Input2
li  $s1,0
lw  $s2,InLenW
li  $t0,0

lw  $t3,($s0)
lw  $t4,($s0)
loopfive:
    beq  $s1,$s2,endfive
    lw   $t1,($s0)
    add  $t0,$t0,$t1
    add  $s0,$s0,4
    add  $s1,$s1,1
    sub  $t5,$t4,$t1
    bltz $t5,Max
    sub  $t5,$t1,$t3
    bltz $t5,Min
    j    loopfive
Max:
    lw  $t4,($s0)
    j   loopfive
Min:
    lw  $t3,($s0)
    j   loopfive
endfive:
    div $t0,$s2
    mflo $t2
    sw  $t4,MAX
    lw  $a0,MAX
    li  $v0,1
    syscall
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
    sw  $t3,MIN
    lw  $a0,MIN
    li  $v0,1
    syscall
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
    sw  $t2,WordAvg
    lw  $a0,WordAvg
    li  $v0,1
    syscall

#################################################################################
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
##################################################################################
#
# Code for Item 7 -- 
# Display the first 25 integers that are divisible by either 7 and 13 (with comma)
#

li $t0,0   #Int
li $t1,25  #last int to check 
li $t2,0   #counter for valid Int
li $t3,13
li $t7,7
loopsix:
   beq  $t2,$t1,endsix
   div  $t0,$t3
   mfhi $t4
   blez $t4,PrintNum
   div  $t0,$t7
   mfhi $t4
   blez $t4,PrintNum
   add $t0,$t0,1
   j   loopsix
PrintNum:
   sw  $t0,ValidInt2
   lw  $a0,ValidInt2
   li  $v0,1
   syscall
   la  $a0,Comma
   li  $v0,4
   syscall
   add $t2,$t2,1
   add $t0,$t0,1
   j   loopsix
endsix:

#################################################################################
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
##################################################################################
#################################################################################
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
##################################################################################
#
# Code for Item 8 -- 
# Repeat step 7 but display the integers in 5 lines with 5 integers with spaces per line
# This must be implemented using a nested loop.
#
li $t0,0   #Int
li $t1,25  #last int to check 
li $t2,0   #counter for valid Int
li $t4,0   #counter for nested loop
li $t5,5   #counter for nested loop
li $t3,13
li $t7,7
loopseven:
    beq  $t2,$t1,endseven
    div  $t0,$t3
    mfhi $t6
    blez $t6,PrintNum2
    div  $t0,$t7
    mfhi $t6
    blez $t6,PrintNum2
    add $t0,$t0,1
    j   loopseven
PrintNum2:
    loopeight:
    beq $t4,$t5,endeight
        sw  $t0,ValidInt2
        lw  $a0,ValidInt2
        li  $v0,1
        syscall
        la  $a0,space
        li  $v0,4
        syscall
        add $t2,$t2,1
        add $t4,$t4,1
        add $t0,$t0,1
        j   loopseven
    endeight:
        la  $a0,Enter
        li  $v0,4
        syscall
        li  $t4,0
        j   loopeight
endseven:

#################################################################################
        la  $a0,Enter   # address of string to print
        li  $v0,4       # system call code for print_str
        syscall                 # print the string
##################################################################################
Exit:
    jr $ra
